package com.dlabs.fm.inventory.domain.product;

import com.dlabs.fm.inventory.core.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class ProductValidator {

    @Autowired
    private ProductRepository repository;

    public static final Integer MAX_LENGTH_TITLE = 512;

    public void validateExistenceById(UUID id) {
        if (!this.repository.existsById(id)) {
            throw new BusinessException("Invalid product id. " + id.toString());
        }
    }

    public void validateTitle(String title) {
        if (title.length() > MAX_LENGTH_TITLE) {
            throw new BusinessException("Title value too large.");
        }
    }
}
