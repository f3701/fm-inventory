package com.dlabs.fm.inventory.domain.product;

import com.dlabs.fm.inventory.core.BusinessException;
import com.dlabs.fm.inventory.core.dtos.ResponseDto;
import com.dlabs.fm.inventory.core.services.DeletableService;
import com.dlabs.fm.inventory.core.services.SearchableService;
import com.dlabs.fm.inventory.shared.UUIDValidator;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.UUID;

@Service
@Transactional(rollbackFor = Exception.class)
@Getter
public class ProductService implements
        SearchableService<
                ProductRepository,
                ProductModel,
                UUID
                >,
        DeletableService<
                ProductRepository,
                ProductModel,
                UUID
                > {

    @Autowired
    private ProductRepository repository;

    @Autowired
    private UUIDValidator uuidValidator;

    @Autowired
    private ProductValidator productValidator;

    @Autowired
    private RestTemplate restTemplate;

    @Value("${drive.urlbase}")
    private String URL_BASE_DRIVE;

    public ResponseDto createProduct(ProductInDto dto) {

        this.productValidator.validateTitle(dto.getTitle());

        ProductModel model = this.fillModelByDto(new ProductModel(), dto);

        model = this.repository.save(model);

        return ResponseDto.builder()
                .data(model)
                .build();
    }

    public ResponseDto updateProduct(ProductInDto dto, String id) {

        // Validations
        this.uuidValidator.validateFormat(id);
        this.productValidator.validateExistenceById(UUID.fromString(id));
        this.productValidator.validateTitle(dto.getTitle());

        ProductModel model = this.repository.getOne(UUID.fromString(id));

        fillModelByDto(model, dto);

        this.repository.save(model);

        return ResponseDto.builder()
                .data(model)
                .build();
    }

    /**
     * Fill model with dto content
     * If the value of dto is null then the previous model value is preserved.
     *
     * @param model of product
     * @param dto   of product
     * @return product model
     */
    private ProductModel fillModelByDto(ProductModel model, ProductInDto dto) {
        model.setTitle(dto.getTitle() != null ? dto.getTitle() : model.getTitle());
        model.setDescription(dto.getDescription() != null ? dto.getDescription() : model.getDescription());
        model.setPrice(dto.getPrice() != null ? dto.getPrice() : model.getPrice());
        return model;
    }

    /**
     * Deletes product and images.
     * It's necessary to call drive microservice.
     *
     * @param productId id
     */
    @Override
    public void deleteById(UUID productId) {

        // Delete product details
        DeletableService.super.deleteById(productId);

        // Delete images
        try {
            this.restTemplate.delete(URL_BASE_DRIVE + "/product-images?productId=" + productId.toString());
        } catch (RestClientException e) {
            e.printStackTrace();
            throw new BusinessException("Could not delete product images.");
        }
    }

}
