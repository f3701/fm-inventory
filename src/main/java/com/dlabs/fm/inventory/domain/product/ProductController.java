package com.dlabs.fm.inventory.domain.product;

import com.dlabs.fm.inventory.core.controllers.DeletableController;
import com.dlabs.fm.inventory.core.controllers.SearchableController;
import com.dlabs.fm.inventory.core.dtos.ResponseDto;
import com.dlabs.fm.inventory.shared.Constants;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;
import java.util.UUID;

@RestController
@Getter
public class ProductController implements
        SearchableController<
                ProductService,
                ProductRepository,
                ProductModel,
                UUID
                >,
        DeletableController<
                ProductService,
                ProductRepository,
                ProductModel,
                UUID
                > {

    @Autowired
    private ProductService productService;

    public static final String CREATE_PRODUCT_ENDPOINT = Constants.BASE_URL + "/products";
    public static final String UPDATE_PRODUCTS_ENDPOINT = Constants.BASE_URL + "/products/{id}";
    public static final String DELETE_PRODUCT_ENDPOINT = Constants.BASE_URL + "/products/{id}";
    public static final String FETCH_PRODUCTS_ENDPOINT = Constants.BASE_URL + "/products";
    public static final String FETCH_PRODUCT_BY_ID_ENDPOINT = Constants.BASE_URL + "/products/{id}";
    public static final String FETCH_PAGE_PRODUCTS_ENDPOINT = Constants.BASE_URL + "/products/page";

    @PostMapping(CREATE_PRODUCT_ENDPOINT)
    public ResponseDto createProduct(@RequestBody @Valid ProductInDto dto) {

        return this.productService.createProduct(dto);

    }

    @Override
    @GetMapping(FETCH_PRODUCT_BY_ID_ENDPOINT)
    public ResponseDto findById(@PathVariable UUID id) {
        return SearchableController.super.findById(id);
    }

    @Override
    @GetMapping(FETCH_PRODUCTS_ENDPOINT)
    public ResponseDto findAll() {

        return this.productService.findAll();

    }

    @PutMapping(UPDATE_PRODUCTS_ENDPOINT)
    public ResponseDto update(@RequestBody ProductInDto dto, @PathVariable String id) {

        return this.productService.updateProduct(dto, id);

    }

    @GetMapping(FETCH_PAGE_PRODUCTS_ENDPOINT)
    ResponseDto getPage(Pageable pageable, ProductSpecification specification ) {

        return this.productService.getPage(pageable, specification);

    }

    @Override
    @DeleteMapping(DELETE_PRODUCT_ENDPOINT)
    public ResponseEntity<?> deleteById(@PathVariable UUID id) {
        this.productService.deleteById(id);
        return ResponseEntity.ok().body(Map.of("message", "Deleted successfully."));
    }

    @Override
    public ProductService getService() {
        return this.productService;
    }

}
