package com.dlabs.fm.inventory.domain.product;

import com.dlabs.fm.inventory.core.dtos.InDto;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

@Builder
@Data
@EqualsAndHashCode(callSuper = false)
public class ProductInDto extends InDto {

    private String title;

    private String description;

    private BigDecimal price;

}


