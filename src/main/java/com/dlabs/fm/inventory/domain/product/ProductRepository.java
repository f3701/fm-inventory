package com.dlabs.fm.inventory.domain.product;

import com.dlabs.fm.inventory.core.repositories.DeletableRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ProductRepository extends
        DeletableRepository<
                ProductModel,
                UUID
                > {
}

