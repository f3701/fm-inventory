package com.dlabs.fm.inventory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class InventoryApplication {

	// Starting Point of the Application
	public static void main(String[] args) {
		SpringApplication.run(InventoryApplication.class, args);
	}

}
