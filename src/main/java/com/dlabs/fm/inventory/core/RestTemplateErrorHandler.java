package com.dlabs.fm.inventory.core;

import com.dlabs.fm.inventory.core.dtos.ResponseDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.ResponseErrorHandler;
import java.io.IOException;
import java.lang.reflect.Method;

@Slf4j
public class RestTemplateErrorHandler implements ResponseErrorHandler {

    private final static ObjectMapper mapper = new ObjectMapper();

    @Override
    public void handleError(ClientHttpResponse response) throws IOException {

        if (response.getStatusCode().series() == HttpStatus.Series.SERVER_ERROR) {
            log.error(response.toString());
            throw new RuntimeException("Internal Server Error.");

        } else if (response.getStatusCode().series() == HttpStatus.Series.CLIENT_ERROR) {
            ResponseDto responseDto = mapper.readValue(response.getBody(), ResponseDto.class);
            String message = "";
            try {
                Object data = responseDto.getData();
                Class<?> dataClass = data.getClass(); // It's supposed be Map
                Method method = dataClass.getMethod("get", Object.class);
                message = (String) method.invoke(data, "message");
            } catch (Exception e) {
                log.error("It couldn't be extract message from error.", e);
            }
            throw new BusinessException(message);
        }
    }

    @Override
    public boolean hasError(ClientHttpResponse response) throws IOException {
        return new DefaultResponseErrorHandler().hasError(response);

    }
}
