package com.dlabs.fm.inventory.core.models;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.UUID;

@MappedSuperclass
@SuperBuilder
@Data
@EqualsAndHashCode(callSuper = false)
@ToString(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public abstract class UUIDBaseModel extends BaseModel<UUID> {

    @Id
    @GeneratedValue
    @Column(columnDefinition = "uuid")
    protected UUID id;

}
