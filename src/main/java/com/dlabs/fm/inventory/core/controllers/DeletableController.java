package com.dlabs.fm.inventory.core.controllers;

import com.dlabs.fm.inventory.core.models.BaseModel;
import com.dlabs.fm.inventory.core.repositories.DeletableRepository;
import com.dlabs.fm.inventory.core.services.DeletableService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Map;

public interface DeletableController<
        S extends DeletableService<R, E, ID>,
        R extends DeletableRepository<E, ID>,
        E extends BaseModel<ID>,
        ID> {


    S getService();

    @DeleteMapping("/{id}")
    default ResponseEntity<?> deleteById(@PathVariable ID id) {

        this.getService().deleteById(id);

        return ResponseEntity.ok().body(Map.of("message", "Deleted successfully."));

    }

}
