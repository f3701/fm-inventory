package com.dlabs.fm.inventory.core.services;

import com.dlabs.fm.inventory.core.repositories.BaseRepository;
import com.dlabs.fm.inventory.core.BusinessException;
import com.dlabs.fm.inventory.core.dtos.ResponseDto;
import com.dlabs.fm.inventory.core.models.BaseModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional(rollbackFor = Exception.class)
public interface SearchableService<
        R extends BaseRepository<E, ID>,
        E extends BaseModel<ID>,
        ID
        >
        extends BaseService<R, E, ID> {

    int MAX_SIZE_RESULTS = 256;

    default ResponseDto findAll() {

        if (this.getRepository().findAll().size() > MAX_SIZE_RESULTS) {
            throw new BusinessException("The amount of results exceeds the maximum supported, use paging instead.");
        }

        List<E> results = this.getRepository().findAll();

        return ResponseDto.builder()
                .data(results)
                .build();
    }

    default ResponseDto findById(ID id) {

        E result = this.getRepository().findById(id)
                .orElseThrow(() -> new BusinessException(String.format("Id %s not exist.", id)));

        return ResponseDto.builder()
                .data(result)
                .build();

    }

    default ResponseDto getPage(Pageable pageable, Specification<E> specification){

        Page<E> page = this.getRepository().findAll(specification, pageable);
        return ResponseDto.builder()
                .data(page)
                .build();

    }

}
