package com.dlabs.fm.inventory.core.services;

import com.dlabs.fm.inventory.core.repositories.DeletableRepository;
import com.dlabs.fm.inventory.core.BusinessException;
import com.dlabs.fm.inventory.core.models.BaseModel;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Transactional(rollbackFor = Exception.class)
public interface DeletableService<
        R extends DeletableRepository<E, ID>,
        E extends BaseModel<ID>,
        ID
        >
        extends BaseService<R, E, ID> {

    default void deleteById(ID id) {

        if (!this.getRepository().existsById(id)) {
            throw new BusinessException(String.format("Id %s not exist", id));
        }

        this.getRepository().deleteById(id, new Date(), "System");

    }

}
