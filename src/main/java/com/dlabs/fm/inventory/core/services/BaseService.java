package com.dlabs.fm.inventory.core.services;

import com.dlabs.fm.inventory.core.repositories.BaseRepository;
import com.dlabs.fm.inventory.core.models.BaseModel;

public interface BaseService<
        R extends BaseRepository<E, I>,
        E extends BaseModel<I>,
        I
        > {

    R getRepository();

}
