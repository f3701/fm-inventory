package com.dlabs.fm.inventory.core.controllers;

import com.dlabs.fm.inventory.core.dtos.ResponseDto;
import com.dlabs.fm.inventory.core.models.BaseModel;
import com.dlabs.fm.inventory.core.repositories.BaseRepository;
import com.dlabs.fm.inventory.core.services.SearchableService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

public interface SearchableController<
        S extends SearchableService<R, E, ID>,
        R extends BaseRepository<E, ID>,
        E extends BaseModel<ID>,
        ID
        > {

    S getProductService();

    @GetMapping("/{id}")
    default ResponseDto findById(@PathVariable ID id) {
        return this.getProductService().findById(id);
    }

    @GetMapping("")
    default ResponseDto findAll() {
        return this.getProductService().findAll();
    }

}
