package com.dlabs.fm.inventory.core.repositories;

import com.dlabs.fm.inventory.core.models.BaseModel;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;

import java.util.Date;

@NoRepositoryBean
public interface DeletableRepository<E extends BaseModel<ID>, ID> extends BaseRepository<E, ID> {


    /**
     * Delete record (soft-delete)
     *
     * @param id record
     */
    @Query("UPDATE #{#entityName} e SET e.deletedAt = :date, e.deletedBy = :user WHERE e.id = :id ")
    @Modifying
    void deleteById(@Param("id") ID id, @Param("date") Date date, @Param("user") String user);

}

