package com.dlabs.fm.inventory.core.dtos;

import lombok.Data;

import java.util.List;

/**
 * Represents page of response
 */

@Data
public class PageDto<T> {
    private List<T> content;

    // General Metadata
    private Integer totalPages;
    private Integer totalElements;

    // Page Metadata
    private Integer numberOfElements;
    private Integer size;
    private Integer number;
    private boolean first;
    private boolean last;
    private boolean empty;
}
