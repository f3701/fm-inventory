package com.dlabs.fm.inventory.core.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.Where;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.io.Serializable;
import java.util.Date;

@MappedSuperclass
@SuperBuilder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Where(clause = "deleted_at IS NULL")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public abstract class BaseModel<ID> implements Serializable {

    protected abstract void setId(ID id);
    protected abstract ID getId();

    @Column(name = "created_at")
    @JsonIgnore
    protected Date createdAt;

    @Column(name = "created_by")
    @JsonIgnore
    protected String createdBy;

    @Column(name = "deleted_at")
    @JsonIgnore
    protected Date deletedAt;

    @Column(name = "deleted_by")
    @JsonIgnore
    protected String deletedBy;

    @Column(name = "updated_last_at")
    @JsonIgnore
    protected Date updatedLastAt;

    @Column(name = "updated_last_by")
    @JsonIgnore
    protected String updatedLastBy;

    // Updating Audit fields
    @PrePersist
    public void prePersist() {
        this.setCreatedAt(new Date());
        this.setCreatedBy("SYSTEM");
    }
    // Updating Audit fields
    @PreUpdate
    public void preUpdate() {
        this.setUpdatedLastAt(new Date());
        this.setUpdatedLastBy("SYSTEM");
    }
}
