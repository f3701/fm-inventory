ALTER TABLE inventory.products
    ADD COLUMN price NUMERIC (12, 4) DEFAULT NULL;

